import { CANCEL, SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, SHOW_EDIT_BAR, HIDE_EDIT_BAR } from "./editActionsTypes";

export const cancel = () => ({
    type: CANCEL
});

export const setMessageId = (id) => ({
    type: SET_CURRENT_MESSAGE_ID,
    payload: {
        id
    }
});

export const dropMessageId = () => ({
    type: DROP_CURRENT_MESSAGE_ID
});

export const showEdit = () => ({
    type: SHOW_EDIT_BAR,
});

export const hideEdit = () => ({
    type: HIDE_EDIT_BAR
});