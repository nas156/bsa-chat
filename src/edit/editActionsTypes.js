export const HIDE_EDIT_BAR = "HIDE_EDIT_BAR";
export const SHOW_EDIT_BAR = "SHOW_EDIT_BAR";
export const CANCEL = "CANCEL";
export const SUBMIT_EDITENG = "SUBMIT_EDITENG";
export const SET_CURRENT_MESSAGE_ID = "SET_CURRENT_MESSAGE_ID";
export const DROP_CURRENT_MESSAGE_ID = "DROP_CURRENT_MESSAGE_ID";
