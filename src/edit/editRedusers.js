import { HIDE_EDIT_BAR, SHOW_EDIT_BAR, SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID } from "./editActionsTypes";

const ititialState = {
    id: '',
    isHidden: true,
    startMessage: ''
};

export default function (state = ititialState, action) {
    switch (action.type) {

        case HIDE_EDIT_BAR: {
            return ({
                ...state,
                isHidden: true,
                startMessage: ''
            });
        }

        case SHOW_EDIT_BAR: {
            return ({
                ...state,
                isHidden: false,
            });
        }

        case SET_CURRENT_MESSAGE_ID: {
            const { id } = action.payload;
            return ({
                ...state,
                id: id
            });
        }

        case DROP_CURRENT_MESSAGE_ID: {
            return ({
                ...state,
                id: ''
            });
        }

        default:
            return state;
    }

}
