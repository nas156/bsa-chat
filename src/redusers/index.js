
import chat from '../chat/chatRedusers';
import edit from '../edit/editRedusers';
import { combineReducers } from 'redux';


const rootReducer = combineReducers({chat, edit})

export default rootReducer;