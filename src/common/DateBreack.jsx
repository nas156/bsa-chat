import React from 'react';

class DateBreacker extends React.Component{
    render(){
        return(
            <div className='breack'>
                <div className ='line'></div>
                <div>{this.props.date}</div>
                <div className ='line'></div>
            </div>
        );
    }
}

export default DateBreacker;