import React from 'react';
import './Chat.css';
import logo from './logo.jpg'
import Header from './common/Header';
import ChatPanel from './chat/components/ChatPanel';
import Footer from './common/Footer';
import EditPanel from './edit/EditPanel';



class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true };
  }

  componentDidMount() {
    this.setState({ isLoading: false });
  }

  render() {
    return (
      this.state.isLoading ? <div className="loader"></div> :
        <div className="chat">
          <Header imageSrc={logo} />
          <ChatPanel />
          <EditPanel />
          <Footer />
        </div>
    );
  }
}

export default Chat;
